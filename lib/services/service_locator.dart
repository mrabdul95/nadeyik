import 'package:get_it/get_it.dart';
import 'package:nadeyik/services/database.dart';
import 'package:nadeyik/services/purchase_iap.dart';
import 'package:nadeyik/services/purchases_service.dart';
GetIt locator = GetIt.instance;
void setUpLocator(){
  locator.registerSingleton<DataBaseService>(DataBaseService(),signalsReady: true);
  locator.registerSingleton<PurchasesService>(PurchasesService(),signalsReady: true);
}