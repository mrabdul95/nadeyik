import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:nadeyik/models/gym.dart';
import 'package:nadeyik/models/trainer.dart';
import 'package:nadeyik/models/trainingProgram.dart';

class DataBaseService with ChangeNotifier {
  String uid;
  DataBaseService.forUser({this.uid});
  DataBaseService();
  String _trainerId;

  final CollectionReference gymsCollection =
      Firestore.instance.collection("Gyms");

  final CollectionReference trainersCollection =
      Firestore.instance.collection("Trainers");

  final CollectionReference trainingProgramsCollection =
      Firestore.instance.collection("TrainingPrograms");

  final CollectionReference workoutsCollection =
      Firestore.instance.collection("workouts");
  List<Trainer> _trainerListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents
        .map((e) => Trainer(
            details: e.data["details"],
            gender: e.data["gender"],
            id: e.data["id"],
            image: e.data["image"],
            specialty: List<String>.from(e.data["specialty"]),
            specialtyAr: List<String>.from(e.data["specialtyAr"]),
            name: e.data["name"],
            detailsAr: e.data["detailsAr"],
            nameAr: e.data["nameAr"]))
        .toList();
  }

  List<TrainingProgram> _trainingProgramListFromSnapshot(
      QuerySnapshot snapshot) {
    try {
      print("trainingProgramListFromSnapshot called");
      return snapshot.documents.map((e) {
        print(e.data["difficulty"]);
        return TrainingProgram(
            nameAr: e.data["nameAr"],
            name: e.data["name"],
            id: e.data["id"],
            description: e.data["description"],
            descriptionAr: e.data["descriptionAr"],
            difficulty: (e.data["difficulty"]),
            trainerId: e.data["trainerId"]);
      }).toList();
    } catch (e) {
      print(e);
    }
  }

  List<Gym> _gymListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      var facilityMap =
          Map<String, Map<String, dynamic>>.from(doc.data["facilities"]);
      List<Facilitie> facilities = facilityMap.entries
          .map((e) => Facilitie(
              name: e.key,
              available: e.value["available"] ? "true" : "false",
              closeHours: e.value["closeTime"],
              openHours: e.value["openTime"]))
          .toList();
      facilities.forEach((element) {
        print(element.available);
      });
      return Gym(
          id: doc.documentID,
          currentPrice: doc.data["currentPrice"],
          oldPrice: doc.data["oldPrice"],
          details: doc.data["details"],
          name: doc.data["name"],
          nameAr: doc.data["nameAr"],
          logo: doc.data["logo"],
          location: doc.data["location"],
          images: List.from(doc.data["image"]),
          gender: doc.data["gender"],
          facilities: facilities,
          equipment: Map<String, String>.from(doc.data["equipment"]));
    }).toList();
  }

  Stream<List<Gym>> get gyms {
    return gymsCollection.snapshots().map(_gymListFromSnapshot);
  }

  Stream<List<TrainingProgram>> getTrainingPrograms(String id) {
    try {
      var toreturn = trainingProgramsCollection
          .where("trainerId", isEqualTo: id)
          .snapshots()
          .map((_trainingProgramListFromSnapshot));
      print(toreturn);
      return toreturn;
    } catch (error) {
      print(error);
    }
  }

  Stream<List<Trainer>> get trainers {
    return trainersCollection.snapshots().map(_trainerListFromSnapshot);
  }
}
