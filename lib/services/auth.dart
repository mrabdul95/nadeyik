import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:nadeyik/models/user.dart';
// import 'package:shoppingcart/services/database.dart';

class AuthService with ChangeNotifier{
  //creating a firebase auth instance
  final FirebaseAuth _auth = FirebaseAuth.instance;
  bool isAnon=true;
  //create a user object based on firebase user using our custome user model
  // User _userFromFirebaseUser(FirebaseUser firebaseUserser) {
  //   return firebaseUserser != null
  //       ? User(
  //           uid: firebaseUserser.uid,
  //         )
  //       : null;
  // }

  //auth change user stream

  Stream<FirebaseUser> get user {
    return _auth.onAuthStateChanged; //returniing  the auth stream
    //that stream is going to give us a stream that returns a firebaseuser
    //so we map it to our custome user using our helper method.
    // .map((FirebaseUser fireBaseUser) => _userFromFirebaseUser(fireBaseUser));
    //map short cut
    // .map(_userFromFirebaseUser);
  }

  //sign in anonmysly
  Future signInAnon() async {
    try {
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;
      //this will return our custome user object instead of the firebase user
      // return _userFromFirebaseUser(user);
      
      isAnon=true;
      notifyListeners();
      return user;
    } catch (error) {
      print(error.toString());
      return null;
    }
  }

  //sign in with  email and password

  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;
      print(email + ": signed in succesfully");

      // return _userFromFirebaseUser(user);
      isAnon=false;
      notifyListeners();
      return user;
    } catch (error) {
      print("sign in error: " + error.toString());
      return null;
    }
  }

  //upgrade a ussesr
  Future upgradeAnon(
      String email, String password, String name, String phoneNumber) async {
    print("upgrade anon called");
    try {
      var currentuser = await _auth.currentUser();
      print(currentuser.isAnonymous);
      var credential =
          EmailAuthProvider.getCredential(email: email, password: password);
      if (currentuser.isAnonymous) {
        var result = await (currentuser.linkWithCredential(credential));
        if (result.user != null) {
          await Firestore.instance
              .collection("users")
              .document(result.user.uid)
              .setData({
            'uid': result.user.uid,
            'name': name,
            'email': result.user.email,
            'isEmailVerified': false,
            'photoUrl': null,
            'phoneNumber': phoneNumber
          });
          print(result.user.email + " has been upgraded to not anon");
          isAnon=false;
          notifyListeners();
          return result.user;
        }
      }
    } catch (error) {
      print(error);
      return null;
    }
  }

  //register with email and password
  Future signUpWithEmailAndPassword(
      String email, String password, String name, String phoneNumber) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      FirebaseUser user = result.user;
      //create a  user record in firestore with the default values.
      await Firestore.instance.collection("users").document(user.uid).setData({
        'uid': user.uid,
        'name': name,
        'email': user.email,
        'isEmailVerified': false,
        'photoUrl': null,
        'phoneNumber': phoneNumber
      });

      print(email + "signed up");
      // return _userFromFirebaseUser(user);
      isAnon=false;
      notifyListeners();
      return user;
    } catch (error) {
      print(error.toString());
      return null;
    }
  }
  //signout

  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (error) {
      print(error.toString());
    }
  }
}
