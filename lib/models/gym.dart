import 'package:flutter/foundation.dart';

class Gym {
  final String id;
  final String name;
  final String nameAr;
  final String gender;
  final List<String> images;
  final String logo;
  final String currentPrice;
  final String oldPrice;
  final Map<String, dynamic> equipment;
  final List<Facilitie> facilities;
  final String details;
  final String location;

  Gym(
      {@required this.id,
      @required this.name,
      @required this.gender,
      @required this.images,
      @required this.logo,
      @required this.nameAr,
      @required this.facilities,
      @required this.currentPrice,
      @required this.oldPrice,
      @required this.equipment,
      @required this.details,
      @required this.location});
}

class Facilitie {
  final String name;
  final String openHours;
  final String closeHours;
  final String available;

  Facilitie({this.name, this.available, this.closeHours, this.openHours});
}

// class GymList {
//   List<Gym> _gymList = [];

//   GymList.fromList(@required this._gymList);

//   List<Gym> get gymList {
//     return [..._gymList];
//   }
// }
