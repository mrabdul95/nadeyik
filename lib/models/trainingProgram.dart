import 'package:flutter/cupertino.dart';

class TrainingProgram {
  final String id;
  final String name;
  final String nameAr;
  final String trainerId;
  final int difficulty;
  final String description;
  final String descriptionAr;

  TrainingProgram({
    @required this.description,
    @required this.descriptionAr,
    @required this.difficulty,
    @required this.name,
    @required this.nameAr,
    @required this.id,
    @required this.trainerId,
  });
}