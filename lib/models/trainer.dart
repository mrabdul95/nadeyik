import 'package:flutter/foundation.dart';

class Trainer {
  final String id;
  final String name;
  final String nameAr;
  final List<String> specialty;
  final List<String> specialtyAr;
  final String gender;
  final String image;
  final String details;
  final String detailsAr;

  Trainer({
    @required this.id,
    @required this.name,
    @required this.gender,
    @required this.image,
    @required this.specialty,
    @required this.specialtyAr,
    @required this.nameAr,
    @required this.details,
    @required this.detailsAr
  });
}



class Workout {
  final String id;
  final String trainerId;
  final String trainerProgramId;
  final String name;
  final String description;
  final String videoUrl;
  final String reps;
  final String rounds;
  final String rest;
  final String duration; // for cardio, becomes null for weights
  Workout({
     @required this.id,
     @required this.trainerId,
     @required this.description,
     @required this.duration,
     @required this.name,
     @required this.reps,
     @required this.rest,
     @required this.rounds,
     @required this.trainerProgramId,
     @required this.videoUrl,
  });
}
