import 'package:flutter/material.dart';

import 'package:nadeyik/screens/authintication/register.dart';
import 'package:nadeyik/screens/authintication/sign_in.dart';

class AuthenticateScreen extends StatefulWidget {
  static const routeName = "AuthenticateScreen";
  @override
  _AuthenticateScreenState createState() => _AuthenticateScreenState();
}

class _AuthenticateScreenState extends State<AuthenticateScreen> {
  
  bool showSignIn = true;
  
  Widget animatedWidget;

  ///a function that will get passed to the sign in  and reg  pagges to toggle which page to display
  void toggleSignIn() {
    setState(() {
      
      showSignIn = !showSignIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      showSignIn
          ? animatedWidget = SignIn(
              toggleView: toggleSignIn,
            )
          : animatedWidget = Register(
              toggleView: toggleSignIn,
            );
    });
    return Scaffold(

          body: AnimatedSwitcher(
        transitionBuilder: (child, animation)=>ScaleTransition(child: child,scale: animation,),
        duration: const Duration(milliseconds: 300),
        child: animatedWidget,
      ),
    );
  }
}
