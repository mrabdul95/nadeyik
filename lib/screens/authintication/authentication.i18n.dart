import 'package:i18n_extension/i18n_extension.dart';

extension Localizatiion on String {
  static var _t = Translations("en") +
      {
        "en": "Sign Up",
        "ar": "تسجيل حساب",
      } +
      {
        "en": "Sign in",
        "ar": "تسجيل دخول",
      } +
      {
        "en": "Sign Up",
        "ar": "تسجيل حساب",
      } +
      {
        'en': 'Name:',
        'ar': 'أسم:',
      } +
      {
        'en': 'Email:',
        'ar': 'أيميل:',
      } +
      {
        'en': 'Password:',
        'ar': 'كلمة المرور:',
      } +
      {
        'en': 'Confirm Password:',
        'ar': 'تأكيد كلمة المرور:',
      } +
      {
        'en': 'Phone Number:',
        'ar': ':رقم الهاتف',
      } +
      {
        'en': 'Already have an account?',
        'ar': 'لديك حساب؟',
      } +
      {'en': 'Please enter your name', 'ar': 'الرجاء ادخال اسمك'} +
      {
        'en': 'invalid email',
        'ar': 'الايميل غير صحيح',
      } +
      {
        'en': 'please enter your password',
        'ar': 'الرجاء ادخال كلمة المرور',
      } +
      {
        'en': 'please enter your Phone number',
        'ar': 'الرجاء ادخال رقم الهاتف',
      } +
      {
        'en': 'Change language',
        'ar': 'تغيير اللغة',
      } +
      {
        'en': 'skip',
        'ar': 'تخطي',
      } +
      {
        'en': "don't have an account?",
        'ar': 'ليس لديك حساب؟',
      } +
      {
        'en': 'phone number too short',
        'ar': 'رقم الهاتف قصير',
      } +
      {
        'en': 'Phone number too long',
        'ar': 'رقم الهاتف طويل',
      };

  String get i18n => localize(this, _t);
}
