import 'package:country_code_picker/country_code.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:nadeyik/constants/constants.dart';
import 'package:nadeyik/constants/loading.dart';
import 'package:nadeyik/custom_icons_icons.dart';
import 'package:nadeyik/services/auth.dart';
import 'package:provider/provider.dart';
import 'authentication.i18n.dart';
import 'package:nadeyik/shared/shared_prefs.dart';
// import 'package:i18n_extension/default.i18n.dart';

class Register extends StatefulWidget {
  static const routeName = "/register";
  final Function toggleView;

  Register({@required this.toggleView});
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  bool loading = false;

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }

  final _formKey = GlobalKey<FormState>();

  String _email;
  String _password;
  String _name;
  String _phoneNnumber;
  String _confirmPassword;
  String _countryCode = "+965";
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    final _auth = Provider.of<AuthService>(context);
    final _sharedPrefs = Provider.of<SharedPrefs>(context);
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                  top: mediaQuery.size.height * 0.05,
                  left: mediaQuery.size.width * 0.25,
                  right: mediaQuery.size.width * 0.25,
                  bottom: mediaQuery.size.height * 0.03),
              child: Image.asset(
                "lib/assets/images/logo.png",
                fit: BoxFit.scaleDown,
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                horizontal: mediaQuery.size.width * 0.1,
              ),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    TextFormField(
                      decoration: textInputDecoration.copyWith(
                        labelText: "Name:".i18n,
                      ),
                      validator: (val) =>
                          val.isEmpty ? "Please enter your name".i18n : null,
                      onSaved: (val) => _name = val,
                    ),
                    SizedBox(
                      height: mediaQuery.size.height * 0.015,
                    ),
                    TextFormField(
                      decoration: textInputDecoration.copyWith(
                          labelText: "Email:".i18n),
                      validator: (val) =>
                          validateEmail(val) ? null : "invalid email".i18n,
                      onSaved: (val) => _email = val,
                    ),
                    SizedBox(
                      height: mediaQuery.size.height * 0.015,
                    ),
                    TextFormField(
                      decoration: textInputDecoration.copyWith(
                          labelText: "Password:".i18n),
                      validator: (val) => val.isEmpty
                          ? "please enter your password".i18n
                          : null,
                      onChanged: (val) => _password = val,
                    ),
                    SizedBox(
                      height: mediaQuery.size.height * 0.015,
                    ),
                    TextFormField(
                      decoration: textInputDecoration.copyWith(
                          labelText: "Confirm Password:".i18n),
                      validator: (val) => val.isEmpty
                          ? "please enter your password".i18n
                          : val == _password
                              ? null
                              : "passwords don't match".i18n,
                      onSaved: (val) => _confirmPassword = val,
                    ),

                    SizedBox(
                      height: mediaQuery.size.height * 0.015,
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          CountryCodePicker(
                              initialSelection: "kw",
                              onChanged: (contryCode) =>
                                  _countryCode = contryCode.dialCode),
                          SizedBox(
                            width: mediaQuery.size.width * 0.586,
                            child: TextFormField(
                              inputFormatters: <TextInputFormatter>[
                                WhitelistingTextInputFormatter.digitsOnly
                              ],
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: false, signed: false),
                              decoration: textInputDecoration.copyWith(
                                  labelText: "Phone Number:".i18n),
                              validator: (val) => val.isEmpty
                                  ? "please enter your Phone number".i18n
                                  : val.length < 7
                                      ? "phone number too short".i18n
                                      : val.length > 15
                                          ? "Phone number too long".i18n
                                          : null,
                              onSaved: (val) =>
                                  _phoneNnumber = "$_countryCode$val",
                            ),
                          ),
                        ],
                      ),
                    ),

                    //button
                    Container(
                      margin:
                          EdgeInsets.only(top: mediaQuery.size.height * 0.03),
                      height: 50.0,
                      child: RaisedButton(
                        onPressed: () async {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            setState(() {
                              loading = true;
                            });
                            dynamic response =
                                await _auth.signUpWithEmailAndPassword(
                                    _email, _password, _name, _phoneNnumber);
                            if (response == null) {
                              setState(() {
                                loading = false;
                              });
                              print("something went wrong");
                            } else {
                              Navigator.pop(context);
                              setState(() {
                                loading = false;
                              });
                              print(
                                  "Email: $_email password: $_password, name: $_name, PhoneNumber: $_phoneNnumber");
                            }
                          }
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80.0)),
                        padding: EdgeInsets.all(0.0),
                        child: loading
                            ? Loading()
                            : Ink(
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      colors: [
                                        Color(0xff374ABE),
                                        Colors.purple[500]
                                      ],
                                      begin: Alignment.centerLeft,
                                      end: Alignment.centerRight,
                                    ),
                                    borderRadius: BorderRadius.circular(30.0)),
                                child: Container(
                                  constraints: BoxConstraints(
                                      maxWidth: 300.0, minHeight: 50.0),
                                  alignment: Alignment.center,
                                  child: Text(
                                    "Sign Up".i18n,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                      ),
                    ),
                    FlatButton(
                      child: Text("Already have an account?".i18n),
                      onPressed: () {
                        widget.toggleView();
                      },
                    ),
                    FlatButton.icon(
                      icon: Icon(CustomIcons.language),
                      label: Text("Change language".i18n),
                      onPressed: () async {
                        await _sharedPrefs.toggleLocale();
                      },
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
