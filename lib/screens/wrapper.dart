import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:nadeyik/screens/authintication/authenticate.dart';
import 'package:nadeyik/screens/home/home.dart';

class Wrapper extends StatelessWidget {
  // final SharedPrefs sharedPrefs;
  // Wrapper({this.sharedPrefs});

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<FirebaseUser>(context);
    return user == null ? AuthenticateScreen() : Home();
    //return either home or authinticate widget dependingg on if the user is signed in or not
    // final user = Provider.of<FirebaseUser>(context); //since the sstream we are listneingg to provides us wwith User, so we can access it here
    // return user == null ? AuthenticateScreen() : Home();
  }
}
