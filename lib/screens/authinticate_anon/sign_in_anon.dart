import 'package:flutter/material.dart';
import 'package:nadeyik/constants/loading.dart';
import 'package:nadeyik/custom_icons_icons.dart';
import 'package:nadeyik/services/auth.dart';
import 'package:nadeyik/shared/shared_prefs.dart';
import 'package:provider/provider.dart';
import 'package:nadeyik/screens/authintication/authentication.i18n.dart';
import '../../constants/constants.dart';

class SignInAnon extends StatefulWidget {
  final Function toggleView;
  final BuildContext ctx;

  SignInAnon({@required this.toggleView, this.ctx});
  @override
  _SignInAnonState createState() => _SignInAnonState();
}

class _SignInAnonState extends State<SignInAnon> {
  bool loading = false;

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }

  final _formKey = GlobalKey<FormState>();

  String _email;
  String _password;
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    final _auth = Provider.of<AuthService>(widget.ctx);
    final _sharedPrefs = Provider.of<SharedPrefs>(context);

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                  top: mediaQuery.size.height * 0.17,
                  left: mediaQuery.size.width * 0.2,
                  right: mediaQuery.size.width * 0.2,
                  bottom: mediaQuery.size.height * 0.03),
              child: Image.asset(
                "lib/assets/images/logo.png",
                fit: BoxFit.scaleDown,
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                horizontal: mediaQuery.size.width * 0.1,
              ),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    TextFormField(
                      decoration: textInputDecoration.copyWith(
                        labelText: "Email:".i18n,
                      ),
                      validator: (val) =>
                          validateEmail(val) ? null : "invalid email".i18n,
                      onSaved: (val) => _email = val,
                    ),
                    SizedBox(
                      height: mediaQuery.size.height * 0.015,
                    ),
                    TextFormField(
                      decoration: textInputDecoration.copyWith(
                          labelText: "Password:".i18n),
                      validator: (val) => val.isEmpty
                          ? "please enter your password".i18n
                          : null,
                      onSaved: (val) => _password = val,
                    ),
                    Container(
                      margin:
                          EdgeInsets.only(top: mediaQuery.size.height * 0.03),
                      height: 50.0,
                      child: RaisedButton(
                        onPressed: loading
                            ? null
                            : () async {
                                if (_formKey.currentState.validate()) {
                                  _formKey.currentState.save();
                                  setState(() {
                                    loading = true;
                                  });
                                  dynamic response =
                                      await _auth.signInWithEmailAndPassword(
                                          _email, _password);
                                  if (response == null) {
                                    setState(() {
                                      loading = false;
                                    });
                                    print("something went wrong");
                                  } else {
                                    Navigator.pop(context);
                                    setState(() {
                                      loading = false;
                                    });
                                    print(
                                        "Email: $_email password: $_password");
                                  }
                                }
                              },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80.0)),
                        padding: EdgeInsets.all(0.0),
                        child: loading
                            ? Loading()
                            : Ink(
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      colors: [
                                        Color(0xff374ABE),
                                        Colors.purple[500]
                                      ],
                                      begin: Alignment.centerLeft,
                                      end: Alignment.centerRight,
                                    ),
                                    borderRadius: BorderRadius.circular(30.0)),
                                child: Container(
                                  constraints: BoxConstraints(
                                      maxWidth: 300.0, minHeight: 50.0),
                                  alignment: Alignment.center,
                                  child: Text(
                                    "Sign in".i18n,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                      ),
                    ),
                    FlatButton(
                      child: Text("don't have an account?".i18n),
                      onPressed: () {
                        widget.toggleView();
                      },
                    ), FlatButton.icon(
                      icon: Icon(CustomIcons.language),
                      label: Text("Change language".i18n),
                      onPressed: () async {
                        await _sharedPrefs.toggleLocale();
                      },
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
