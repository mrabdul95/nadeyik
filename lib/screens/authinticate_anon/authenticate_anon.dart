import 'package:flutter/material.dart';

import 'package:nadeyik/screens/authinticate_anon/register_anon.dart';
import 'package:nadeyik/screens/authinticate_anon/sign_in_anon.dart';
class AuthenticateAnonScreen extends StatefulWidget {
  static const routeName = "AuthenticateAnonScreen";
  @override
  _AuthenticateAnonScreenState createState() => _AuthenticateAnonScreenState();
}

class _AuthenticateAnonScreenState extends State<AuthenticateAnonScreen> {
  
  bool showSignIn = false;
  
  Widget animatedWidget;

  ///a function that will get passed to the sign in  and reg  pagges to toggle which page to display
  void toggleSignIn() {
    setState(() {
      
      showSignIn = !showSignIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    var ctx = Map<String,dynamic>.of(ModalRoute.of(context).settings.arguments)["ctx"] as BuildContext;
    setState(() {
      showSignIn
          ? animatedWidget = SignInAnon(
              toggleView: toggleSignIn,
              ctx:ctx
            )
          : animatedWidget = RegisterAnon(
              toggleView: toggleSignIn,
              ctx:ctx
            );
    });
    return Scaffold(

          body: AnimatedSwitcher(
        transitionBuilder: (child, animation)=>ScaleTransition(child: child,scale: animation,),
        duration: const Duration(milliseconds: 300),
        child: animatedWidget,
      ),
    );
  }
}
