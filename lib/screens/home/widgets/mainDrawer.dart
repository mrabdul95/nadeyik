import 'package:flutter/material.dart';
import 'package:nadeyik/custom_icons_icons.dart';
import 'package:nadeyik/screens/authinticate_anon/authenticate_anon.dart';
import 'package:nadeyik/screens/home/gyms/gym_provider.dart';
import 'package:nadeyik/screens/home/widgets/drawerItem.dart';
import 'package:nadeyik/services/auth.dart';
import 'package:nadeyik/shared/shared_prefs.dart';
import 'package:provider/provider.dart';
import 'package:nadeyik/screens/home/home.i18n.dart';

class MainDrawer extends StatelessWidget {
  const MainDrawer({
    Key key,
    @required this.authService,
  }) : super(key: key);

  final AuthService authService;

  @override
  Widget build(BuildContext context) {
    final _sharedPrefs = Provider.of<SharedPrefs>(context);

    return Container(
      width: 200,
      child: Drawer(
        elevation: 5,
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              curve: Curves.ease,
              child: Card(
                child: Container(
                  child: InkWell(
                    customBorder: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                    ),
                    onTap: authService.isAnon
                        ? () {
                            Navigator.pushNamed(
                                context, AuthenticateAnonScreen.routeName,
                                arguments: {"ctx": context});
                          }
                        : () {}, //TODO add  the  transition to the account

                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: 65,
                          child: Image.asset(
                            "lib/assets/images/account.png",
                            fit: BoxFit.fill,
                          ),
                        ),
                        FittedBox(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 15),
                            child: Text(
                                authService.isAnon
                                    ? "Create Account / sign in".i18n
                                    : "Manage Account".i18n,
                                style: TextStyle(
                                  color: Colors.purple,
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                elevation: 10,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50),
                ),
              ),
            ),
            DrawerItem(
              action: () {
                Navigator.pushNamed(context, GymProvider.routeName,
                    arguments: {"authService".i18n: authService});
              },
              icon: Icon(CustomIcons.gym),
              itemText: "Gyms".i18n,
            ),
            DrawerItem(
              action: () {},
              icon: Icon(CustomIcons.trainer),
              itemText: "Trainers".i18n,
            ),
            DrawerItem(
              action: () {},
              icon: Icon(CustomIcons.nutrition),
              itemText: "Nutritionist".i18n,
            ),
            Divider(
              color: Colors.black,
            ),
            DrawerItem(
              action: () {},
              icon: Icon(CustomIcons.contact),
              itemText: "Contact Us".i18n,
            ),
            DrawerItem(
              action: () {},
              icon: Icon(CustomIcons.about),
              itemText: "About Us".i18n,
            ),
            DrawerItem(
              action: () {},
              icon: Icon(CustomIcons.atterbution),
              itemText: "Attribution".i18n,
            ),
            Divider(
              color: Colors.purple,
            ),
            DrawerItem(
              icon: Icon(CustomIcons.language),
              action: () async {
                _sharedPrefs.toggleLocale();
              },
              itemText: "Change Language".i18n,
            ),
            DrawerItem(
              action: () async {
                authService.signOut();
              },
              icon: Icon(CustomIcons.logout),
              itemText: "log out".i18n,
            ),
          ],
        ),
      ),
    );
  }
}
