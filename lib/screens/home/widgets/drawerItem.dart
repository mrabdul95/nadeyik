import 'package:flutter/material.dart';

class DrawerItem extends StatelessWidget {
  final String itemText;
  final Icon icon;
  final Function action;
  const DrawerItem({
    this.action,
    this.icon,
    this.itemText,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).pop();
        action();
      }, //TODO add the log out functionality
      child: ListTile(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[Text(itemText), icon],
        ),
      ),
    );
  }
}
