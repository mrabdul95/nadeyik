import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:nadeyik/screens/home/gyms/gym_provider.dart';
import 'package:nadeyik/screens/home/nutritionist/nutritionist_home.dart';
import 'package:nadeyik/screens/home/trainers/trainer_provider.dart';
import 'package:nadeyik/screens/home/widgets/mainDrawer.dart';
import 'package:nadeyik/services/auth.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:nadeyik/services/database.dart';
import 'package:nadeyik/services/purchase_iap.dart';
import 'package:nadeyik/services/service_locator.dart';
import 'package:provider/provider.dart';
import 'package:nadeyik/screens/home/home.i18n.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool anonWantingToRegState = false;

  @override
  Widget build(BuildContext context) {
    var authService = Provider.of<AuthService>(context);
    var databaseService = Provider.of<DataBaseService>(context);
    print("is Anon? ${authService.isAnon}");
    final homeAppBar = GradientAppBar(
      gradient: LinearGradient(
          colors: [Colors.blue[500], Colors.purple[300], Colors.pink[500]]),
      title: Text(
        "Nadyeik".i18n,
      ),
      actions: <Widget>[],
    );

    return Scaffold(
      appBar: homeAppBar,
      body: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return Container(
            height: constraints.maxHeight,
            width: constraints.maxWidth,
            child: SingleChildScrollView(
              child: AnimationLimiter(
                child: Column(
                  children: AnimationConfiguration.toStaggeredList(
                      duration: const Duration(milliseconds: 1500),
                      childAnimationBuilder: (widget) {
                        return SlideAnimation(
                          horizontalOffset: 50.0,
                          child: FadeInAnimation(
                            child: widget,
                          ),
                        );
                      },
                      children: [
                        HomeMenueItem(
                          text: "Gyms".i18n,
                          constraints: constraints,
                          image: "lib/assets/images/gym.jpg",
                          route: () {
                            Navigator.pushNamed(context, GymProvider.routeName,
                                arguments: {"authService": authService});
                          },
                        ),
                        HomeMenueItem(
                          text: "Nutritionist".i18n,
                          constraints: constraints,
                          image: "lib/assets/images/nutritionst.jpg",
                          route: () {},
                        ),
                        HomeMenueItem(
                            route: () {
                              Navigator.pushNamed(
                                  context, TrainerProvider.routeName,
                                  arguments: {
                                    "dataBaseService": databaseService
                                  });
                            },
                            text: "Trainers".i18n,
                            constraints: constraints,
                            image: "lib/assets/images/trainer.jpg")
                      ]),
                ),
              ),
            ),
          );
        },
      ),
      drawer: MainDrawer(authService: authService),
    );
  }
}

///route is where you wanna go
class HomeMenueItem extends StatelessWidget {
  final BoxConstraints constraints;
  final String text;
  final String image;
  final Function route;
  const HomeMenueItem(
      {Key key, this.constraints, this.text, this.image, this.route})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      child: Padding(
          padding: const EdgeInsets.all(0),
          child: Container(
            height: constraints.maxHeight * 0.32,
            width: constraints.maxWidth,
            padding: EdgeInsets.zero,
            child: Center(
              child: Stack(
                children: <Widget>[
                  Center(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(50),
                      child: FittedBox(
                        child: Image.asset(
                          image,
                          width: constraints.maxWidth * 0.99,
                          height: constraints.maxHeight * 0.32,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: FittedBox(
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        text,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 125,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  )),
                  Material(
                    borderRadius: BorderRadius.circular(50),
                    color: Colors.transparent,
                    child: InkWell(
                      borderRadius: BorderRadius.circular(50),
                      onTap: () {
                        route();
                      },
                    ),
                  )
                ],
              ),
            ),
          )),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
    );
  }
}
