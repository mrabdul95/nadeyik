import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:nadeyik/models/gym.dart';
import 'package:nadeyik/screens/home/gyms/widgets/gym_classes.dart';
import 'package:nadeyik/screens/home/gyms/widgets/gym_description.dart';
import 'package:nadeyik/screens/home/gyms/widgets/gym_facilities.dart';
import 'package:nadeyik/screens/home/home.i18n.dart';

class GymSpecific extends StatefulWidget {
  static const routeName = "/gym-specific";
  @override
  _GymSpecificState createState() => _GymSpecificState();
}

class _GymSpecificState extends State<GymSpecific> {
  @override
  Widget build(BuildContext context) {
    final Gym gym = ModalRoute.of(context).settings.arguments;
    print(gym.name);
    final gymAppBar = GradientAppBar(
      gradient: LinearGradient(
          colors: [Colors.blue[500], Colors.purple[300], Colors.pink[500]]),
      title: Text(gym.name),
      bottom: TabBar(
        tabs: <Widget>[
          Tab(
            text: "Description".i18n,
          ),
          Tab(
            text: "Facilities".i18n,
          ),
          Tab(
            text: "Classes".i18n,
          )
        ],
      ),
      actions: <Widget>[],
    );
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: gymAppBar,
        body: TabBarView(children: <Widget>[
          GymDescription(gym: gym),
          GymFacilities(gym: gym),
          GymClasses(gym: gym)
        ]),
      ),
    );
  }
}
