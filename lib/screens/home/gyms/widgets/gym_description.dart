import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:nadeyik/models/gym.dart';
import 'package:nadeyik/screens/home/home.i18n.dart';

class GymDescription extends StatefulWidget {
  final Gym gym;
  GymDescription({this.gym});

  @override
  _GymDescriptionState createState() => _GymDescriptionState();
}

class _GymDescriptionState extends State<GymDescription> {
  var imgClickedHelper = false;

  void imgClicked() {
    setState(() {
      imgClickedHelper = !imgClickedHelper;
    });
  }

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    String myLocale = Localizations.localeOf(context).languageCode;

    print(widget.gym.facilities.toString());
    return SizedBox.expand(
      child: Container(
        // decoration: BoxDecoration(
        //     gradient: LinearGradient(
        //   begin: Alignment.topLeft,
        //   colors: [
        //     Color.fromARGB(50, 0, 0, 255),
        //     Color.fromARGB(50, 255, 0, 255),
        //   ],
        // )),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              buildCarouselSlider(context, height),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.015,
              ),
              Card(
                elevation: 10,
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage(widget.gym.logo),
                  ),
                  trailing: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Old Price: %s KD".i18n.fill([
                          double.parse(widget.gym.oldPrice).toStringAsFixed(2)
                        ]),
                        style: TextStyle(color: Colors.red),
                      ),
                      Text("Current  Price: %s KD".i18n.fill([
                        double.parse(widget.gym.oldPrice).toStringAsFixed(2)
                      ]))
                    ],
                  ),
                  title: Text(
                      "Gender: %s".i18n.fill([myLocale == "en" ? widget.gym.gender == "female" ? "female" : "male" : widget.gym.gender == "female" ? "أناث" : "ذكور"])),
                ),
              ),
              Card(
                  elevation: 10,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.99,
                    padding: const EdgeInsets.all(15),
                    child: Column(
                      children: <Widget>[
                        Text(
                          "Description:".i18n,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 19),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.015,
                        ),
                        Text(widget.gym.details)
                      ],
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }

  CarouselSlider buildCarouselSlider(BuildContext context, double height) {
    return CarouselSlider(
      options: CarouselOptions(
          autoPlay: true,
          height: imgClickedHelper ? height : 250,
          enlargeCenterPage: imgClickedHelper ? false : true,
          autoPlayCurve: Curves.easeInOutCirc,
          autoPlayAnimationDuration: Duration(seconds: 1),
          viewportFraction: 1.0),
      items: <Widget>[
        ...widget.gym.images
            .map(
              (e) => Card(
                elevation: 10,
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                          width: 1,
                          color: Colors.black,
                          style: BorderStyle.solid)),
                  child: Center(
                    child: InkWell(
                      onTap: imgClicked,
                      child: CachedNetworkImage(
                        imageUrl: e,
                        width: MediaQuery.of(context).size.width * 1,
                        fit: BoxFit.cover,
                        height: imgClickedHelper ? height : 250,
                      ),
                    ),
                  ),
                ),
              ),
            )
            .toList(),
      ],
    );
  }
}
