import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_parallax/flutter_parallax.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nadeyik/models/gym.dart';
import 'package:nadeyik/screens/home/gyms/gym_specific.dart';
import 'package:nadeyik/screens/home/home.i18n.dart';

class GymTile extends StatefulWidget {
  final Gym gym;
  GymTile({this.gym});
  @override
  _GymTileState createState() => _GymTileState();
}

class _GymTileState extends State<GymTile> {
  @override
  Widget build(BuildContext context) {
    String myLocale = Localizations.localeOf(context).languageCode;
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GridTile(
        child: GestureDetector(
            onTap: () {
              //TODO implement on tab for tapping a gym
              Navigator.pushNamed(context, GymSpecific.routeName,
                  arguments: widget.gym);
            },
            child: widget.gym.images[0] == null
                ? CircularProgressIndicator()
                : Parallax.inside(
                    child: CachedNetworkImage(
                      imageUrl: widget.gym.images[0],
                      progressIndicatorBuilder: (ctx, string,
                              downloadProgress) =>
                          Center(child: SpinKitRipple(color: Colors.purple)),
                      fit: BoxFit.cover,
                      height: 400.0,
                    ),
                    mainAxisExtent: 150)),
        footer: Container(
          height: 55,
          child: GridTileBar(
              backgroundColor: Colors.white30,
              leading: Row(
                children: <Widget>[
                  CircleAvatar(
                    backgroundImage: NetworkImage(widget.gym.logo),
                  )
                ],
              ),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(myLocale == "en" ? widget.gym.name : widget.gym.nameAr),
                  Text(myLocale == "en"
                      ? widget.gym.gender == "female" ? "female" : "male"
                      : widget.gym.gender == "female" ? "أناث" : "ذكور")
                ],
              ),
              trailing: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    "%s KD".i18n.fill([widget.gym.oldPrice]),
                    style: TextStyle(
                        decoration: TextDecoration.lineThrough,
                        color: Theme.of(context).errorColor),
                  ),
                  Text(
                    "%s KD".i18n.fill([widget.gym.currentPrice]),
                  )
                ],
              )),
        ),
      ),
    );
  }
}
