import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nadeyik/models/gym.dart';

class GymClasses extends StatefulWidget {
  final Gym gym;
  GymClasses({this.gym});
  @override
  _GymClassesState createState() => _GymClassesState();
}

class _GymClassesState extends State<GymClasses> {
  @override
  DateTime selectedDate = DateTime.now();
  void changeSelected(DateTime selected) {
    setState(() {
      selectedDate = selected;
    });
  }

  Widget build(BuildContext context) {
    DateTime date1 = DateTime.now();
    DateTime date2 = date1.add(Duration(days: 1));
    DateTime date3 = date1.add(Duration(days: 2));
    DateTime date4 = date1.add(Duration(days: 3));
    DateTime date5 = date1.add(Duration(days: 4));
    DateTime date6 = date1.add(Duration(days: 5));
    DateTime date7 = date1.add(Duration(days: 6));
    var mediaQuery = MediaQuery.of(context);
    return Column(children: <Widget>[
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          DatePick(
              mediaQuery: mediaQuery,
              date: date1,
              selected: selectedDate.day == date1.day ? true : false,
              changeSelected: changeSelected),
          DatePick(
              mediaQuery: mediaQuery,
              date: date2,
              selected: selectedDate.day == date2.day ? true : false,
              changeSelected: changeSelected),
          DatePick(
              mediaQuery: mediaQuery,
              date: date3,
              selected: selectedDate.day == date3.day ? true : false,
              changeSelected: changeSelected),
          DatePick(
              mediaQuery: mediaQuery,
              date: date4,
              selected: selectedDate.day == date4.day ? true : false,
              changeSelected: changeSelected),
          DatePick(
              mediaQuery: mediaQuery,
              date: date5,
              selected: selectedDate.day == date5.day ? true : false,
              changeSelected: changeSelected),
          DatePick(
              mediaQuery: mediaQuery,
              date: date6,
              selected: selectedDate.day == date6.day ? true : false,
              changeSelected: changeSelected),
          DatePick(
              mediaQuery: mediaQuery,
              date: date7,
              selected: selectedDate.day == date7.day ? true : false,
              changeSelected: changeSelected),
        ],
      ),
    ]);
  }
}

class DatePick extends StatelessWidget {
  const DatePick(
      {Key key,
      @required this.mediaQuery,
      @required this.date,
      @required this.changeSelected,
      @required this.selected})
      : super(key: key);

  final MediaQueryData mediaQuery;
  final DateTime date;
  final bool selected;
  final Function changeSelected;

  @override
  Widget build(BuildContext context) {
    String myLocale = Localizations.localeOf(context).languageCode;

    return InkWell(
      onTap: () {
        changeSelected(date);
      },
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
            side: BorderSide(
                width: 0.75, color: selected ? Colors.purple : Colors.black38)),
        child: Container(
          width: mediaQuery.size.width * 0.115,
          height: mediaQuery.size.height * 0.07,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                DateFormat("EE", myLocale).format(date),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 11,
                  color: selected ? Colors.purple : Colors.black,
                ),
              ),
              Text(
                DateFormat(myLocale == "en" ? "d/M" : "M/d", myLocale)
                    .format(date),
                style: TextStyle(
                  color: selected ? Colors.purple : Colors.black,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
