import 'package:flutter/material.dart';
import 'package:nadeyik/models/gym.dart';
import 'package:nadeyik/screens/home/home.i18n.dart';

class GymFacilities extends StatelessWidget {
  final Gym gym;
  GymFacilities({this.gym});
  @override
  Widget build(BuildContext context) {
    var facility = [...gym.facilities];
    facility.removeWhere((element) => element.available == "false");
    var mediaQuery = MediaQuery.of(context);
    return SizedBox.expand(
      child: Container(
        child: SingleChildScrollView(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                Text(
                  "Facilities".i18n,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                ),
                Container(
                  // height: facility.length * 70 > mediaQuery.size.height * 0.5
                  //     ? mediaQuery.size.height * 0.5
                  //     : facility.length * 70.0,
                  height: mediaQuery.size.height*0.75,
                  width: mediaQuery.size.width * 0.90,
                  child: ListView.builder(
                    itemCount: facility.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Card(
                        elevation: 5,
                        child: ListTile(
                          title: Text(
                            facility[index].name,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          leading: CircleAvatar(
                            child: FittedBox(
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Text(
                                  DateTime.now().isBefore(DateTime.parse(
                                              facility[index].closeHours)) &&
                                          DateTime.now().isAfter(DateTime.parse(
                                              facility[index].openHours))
                                      ? "Opened".i18n
                                      : "Closed".i18n,
                                  style: TextStyle(color: Colors.black),
                                ),
                              ),
                            ),
                            backgroundColor: DateTime.now().isBefore(
                                        DateTime.parse(
                                            facility[index].closeHours)) &&
                                    DateTime.now().isAfter(DateTime.parse(
                                        facility[index].openHours))
                                ? Colors.green
                                : Colors.red,
                          ),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                  "Opens At: %s ".i18n.fill([TimeOfDay.fromDateTime(DateTime.parse(facility[index].openHours)).format(context)])),
                              Text(
                                  "Closes At: %s ".i18n.fill([TimeOfDay.fromDateTime(DateTime.parse(facility[index].closeHours)).format(context)])),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
                // Text("Equipments".i18n),
                // Container(
                //   width: mediaQuery.size.width * 0.9,
                //   child: Card(
                //     elevation: 10,
                //     child: DataTable(
                //         columns: [
                //           DataColumn(
                //             label: Text(
                //               "Device",
                //               style: TextStyle(
                //                   fontWeight: FontWeight.bold, fontSize: 15),
                //             ),
                //           ),
                //           DataColumn(
                //               label: Text("Count",
                //                   style: TextStyle(
                //                       fontWeight: FontWeight.bold,
                //                       fontSize: 15)))
                //         ],
                //         rows: gym.equipment.entries.map((entrie) {
                //           return DataRow(cells: [
                //             DataCell(Text(entrie.key)),
                //             DataCell(Text(entrie.value))
                //           ]);
                //         }).toList()),
                //   ),
                // ),
              ],
            )),
      ),
    );
  }
}
