import 'package:flutter/material.dart';
import 'package:nadeyik/screens/home/gyms/gym_home.dart';
import 'package:nadeyik/services/auth.dart';
import 'package:nadeyik/services/database.dart';
import 'package:nadeyik/services/service_locator.dart';
import 'package:provider/provider.dart';

class GymProvider extends StatelessWidget {
  static const routeName = "gymHome";

  @override
  Widget build(BuildContext context) {
    AuthService authService = Map<String, dynamic>.of(
        ModalRoute.of(context).settings.arguments)["authService"];

    return StreamProvider.value(
      value: locator<DataBaseService>().gyms,
      child: ChangeNotifierProvider.value(
        value: authService,
        child: GymHome(),
      ),
    );
  }
}
