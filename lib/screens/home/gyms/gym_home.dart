import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:nadeyik/models/gym.dart';
import 'package:nadeyik/screens/home/gyms/widgets/gym_tile.dart';
import 'package:nadeyik/services/auth.dart';
import 'package:provider/provider.dart';
import 'package:nadeyik/screens/home/home.i18n.dart';

class GymHome extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    var gymList = Provider.of<List<Gym>>(context) ?? [];
    var authService = Provider.of<AuthService>(context);
    final gymAppBar = GradientAppBar(
      gradient: LinearGradient(
          colors: [Colors.blue[500], Colors.purple[300], Colors.pink[500]]),
      title: Text("Gyms".i18n),
      
    );

    return Scaffold(
      appBar: gymAppBar ,
          body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 1,
            childAspectRatio: 5 / 3,
          ),
          semanticChildCount: 5,
          itemBuilder: (ctx, index) {
            return gymList == []
                ? CircularProgressIndicator()
                : Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: GymTile(
                      gym: gymList[index],
                    ),
                  );
          },
          itemCount: gymList.length,
        ),
      ),
    );
  }
}
