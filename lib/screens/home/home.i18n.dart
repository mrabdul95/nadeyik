import 'package:i18n_extension/i18n_extension.dart';

extension Localizatiion on String {
  static var _t = Translations("en") +
      {
        'en': 'Create Account / sign in',
        'ar': 'تسجيل حساب / تسجيل دخول',
      } +
      {
        'en': 'Gyms',
        'ar': 'النوادي',
      } +
      {
        'en': 'Trainers',
        'ar': 'المدربين',
      } +
      {
        'en': 'Nutritionist',
        'ar': 'أخصائي غذائي',
      } +
      {
        'en': 'Contact Us',
        'ar': 'التواصل معنا',
      } +
      {
        'en': 'About Us',
        'ar': 'معلومات عنا',
      } +
      {
        'en': 'Change Language',
        'ar': 'تغيير اللغة',
      } +
      {
        'en': 'Attribution',
        'ar': 'الحقوق',
      } +
      {
        'en': 'Manage Account',
        'ar': 'ادارة الحساب',
      } +
      {
        'en': 'log out',
        'ar': 'تسجيل الخروج',
      } +
      {
        'en': 'Nadyeik',
        'ar': 'ناديك',
      } +
      {
        'en': '%s KD',
        'ar': '%s ك.د',
      } +
      {
        'en': 'Classes',
        'ar': 'الصفوف',
      } +
      {
        'en': 'Facilities',
        'ar': 'المرافق',
      } +
      {
        'en': 'Description',
        'ar': 'الوصف',
      } +
      {
        'en': 'Description:',
        'ar': 'الوصف:',
      } +
      {
        'en': 'Old Price: %s KD',
        'ar': 'السعر القديم %s د.ك',
      } +
      {'en': 'Current  Price: %s KD', 'ar': 'السعر الحالي %s د.ك'} +
      {
        'en': 'Gender: %s',
        'ar': 'الجنس: %s',
      } +
      {
        'en': 'Closed',
        'ar': 'مغلق',
      } +
      {
        'en': 'Opens At: %s ',
        'ar': 'يفتح: %s',
      } +
      {'en': 'Closes At: %s ', 'ar': 'يغلق: %s',}+{'en': 'Training Programs',
      'ar': 'البرامج التدريبية'}+{'en': 'Specialty:',
      'ar': 'الاختصاص:'}+{
        'en': 'male',
        'ar': 'ذكر'
      };

  String get i18n => localize(this, _t);
  String fill(List<Object> params) => localizeFill(this, params);
}
