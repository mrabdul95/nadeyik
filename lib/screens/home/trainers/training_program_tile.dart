import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_parallax/flutter_parallax.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nadeyik/models/trainer.dart';
import 'package:nadeyik/models/trainingProgram.dart';
import 'package:nadeyik/screens/home/gyms/gym_specific.dart';
import 'package:nadeyik/screens/home/home.i18n.dart';
import 'package:nadeyik/screens/home/trainers/training_program_provider.dart';
import 'package:nadeyik/screens/home/trainers/training_programs_list.dart';
import 'package:nadeyik/services/purchase_iap.dart';
import 'package:nadeyik/services/service_locator.dart';

class TrainingProgramTile extends StatefulWidget {
  final TrainingProgram trainingProgram;

  TrainingProgramTile({this.trainingProgram});
  @override
  _TrainingProgramTileState createState() => _TrainingProgramTileState();
}

class _TrainingProgramTileState extends State<TrainingProgramTile> {
  @override
  Widget build(BuildContext context) {
    String myLocale = Localizations.localeOf(context).languageCode;
    // Purchases iap = locator<Purchases>();
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GridTile(
        child: GestureDetector(
          onTap: () {
            // TODO implement on tab for tapping a trainer
            // Navigator.pushNamed(context, TrainingProgramProvider.routeName,
            //     arguments: widget.trainer.id);
            
          },
          child: Text(widget.trainingProgram.name),

          //     child: widget.trainingProgram.name == null
          //         ? CircularProgressIndicator()
          //         // : Parallax.inside(
          //         //     child: CachedNetworkImage(
          //         //       imageUrl: widget.trainer.image,
          //         //       progressIndicatorBuilder: (ctx, string,
          //         //               downloadProgress) =>
          //         //           Center(child: SpinKitRipple(color: Colors.purple)),
          //         //       fit: BoxFit.cover,
          //         //       height: 400.0,
          //         //     ),
          //         //     mainAxisExtent: 150)
        ),
        footer: Container(
          height: 55,
          child: GridTileBar(
              backgroundColor: Colors.black45,
              leading: Row(
                children: <Widget>[
                  // CircleAvatar(
                  //   // backgroundImage: NetworkImage(widget.trainer),
                  // )
                ],
              ),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(myLocale == "en"
                      ? widget.trainingProgram.name
                      : widget.trainingProgram.nameAr),
                  // Text(myLocale == "en"
                  //     ? widget.trainer.gender == "female" ? "female" : "male"
                  //     : widget.trainer.gender == "female" ? "أمرأه" : "رجل")
                ],
              ),
              trailing: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    myLocale == "en"
                        ? widget.trainingProgram.description
                        : widget.trainingProgram.descriptionAr,
                    style: TextStyle(color: Colors.white),
                  ),
                  // Text(
                  //   "%s KD".i18n.fill([widget.gym.oldPrice]),
                  //   style: TextStyle(
                  //       decoration: TextDecoration.lineThrough,
                  //       color: Theme.of(context).errorColor),
                  // ),
                  // Text(
                  //   "%s KD".i18n.fill([widget.gym.currentPrice]),
                  // )
                ],
              )),
        ),
      ),
    );
  }
}
