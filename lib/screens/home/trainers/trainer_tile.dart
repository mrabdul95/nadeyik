import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_parallax/flutter_parallax.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nadeyik/models/trainer.dart';
import 'package:nadeyik/screens/home/gyms/gym_specific.dart';
import 'package:nadeyik/screens/home/home.i18n.dart';
import 'package:nadeyik/screens/home/trainers/training_program_provider.dart';
import 'package:nadeyik/screens/home/trainers/training_programs_list.dart';

class TrainerTile extends StatefulWidget {
  final Trainer trainer;

  TrainerTile({this.trainer});
  @override
  _TrainerTileState createState() => _TrainerTileState();
}

class _TrainerTileState extends State<TrainerTile> {
  
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    String myLocale = Localizations.localeOf(context).languageCode;
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GridTile(
        child: widget.trainer.image == null
            ? CircularProgressIndicator()
            : InkWell(
                onTap: () {
                  Navigator.pushNamed(
                      context, TrainingProgramProvider.routeName,
                      arguments: widget.trainer);
                },
                child: Parallax.inside(
                    child: CachedNetworkImage(
                      imageUrl: widget.trainer.image,
                      progressIndicatorBuilder: (ctx, string,
                              downloadProgress) =>
                          Center(child: SpinKitRipple(color: Colors.purple)),
                      fit: BoxFit.cover,
                      height: 400.0,
                    ),
                    mainAxisExtent: 150),
              ),
        footer: Container(
            height: 100,
            child: GridTileBar(
              backgroundColor: Colors.black45,
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(myLocale == "en"
                          ? widget.trainer.name
                          : widget.trainer.nameAr,style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 15),),
                      Text(myLocale == "en"
                          ? widget.trainer.gender == "female"
                              ? "female"
                              : "male"
                          : widget.trainer.gender == "female"
                              ? "أمرأه"
                              : "رجل",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 15),),
                    ],
                  ),
                  Center(

                    child: Container(
                      
                      
                      child: Column(
                        children: <Widget>[Text(
                        
                        "Specialty:".i18n,
                        style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 15),
                      ),
                      SizedBox(height: 1,),
                        
                          ...myLocale == "en"? 
                          widget.trainer.specialty.map((e) => Text(e,style: TextStyle(color: Colors.white,fontSize: 12),)).toList():
                          widget.trainer.specialtyAr.map((e) => Text(e,style: TextStyle(color: Colors.white,fontSize: 12),)).toList(),
                        
                        ],
                                            
                        ),
                      ),
                  ),
                  
                ],
              ),
            )),
      ),
    );
  }
}
