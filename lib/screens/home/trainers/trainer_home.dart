import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:nadeyik/models/trainer.dart';
import 'trainer_tile.dart';
import 'package:provider/provider.dart';
import 'package:nadeyik/screens/home/home.i18n.dart';


class TrainerHome extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {

    var _trainers=(Provider.of<List<Trainer>>(context))??[];
     final trainerAppBar = GradientAppBar(
      gradient: LinearGradient(
          colors: [Colors.blue[500], Colors.purple[300], Colors.pink[500]]),
      title: Text("Gyms".i18n),);

    return Scaffold(appBar: trainerAppBar ,
          body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 20,
            crossAxisSpacing: 20,
            childAspectRatio: 7 / 10,
          ),
          semanticChildCount: 5,
          itemBuilder: (ctx, index) {
            return _trainers == []
                ? CircularProgressIndicator()
                : Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: TrainerTile(
                      trainer: _trainers[index],
                    ),
                  );
          },
          itemCount: _trainers.length,
        ),
      ),
    );
  }
  
}
