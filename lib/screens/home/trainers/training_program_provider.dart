import 'package:flutter/material.dart';
import 'package:nadeyik/models/trainer.dart';
import 'package:nadeyik/models/trainingProgram.dart';
import 'package:nadeyik/screens/home/trainers/specafic_trainer.dart';
import 'package:nadeyik/screens/home/trainers/trainer_home.dart';
import 'package:nadeyik/screens/home/trainers/training_programs_list.dart';
import 'package:nadeyik/services/database.dart';
import 'package:nadeyik/services/service_locator.dart';
import 'package:provider/provider.dart';

class TrainingProgramProvider extends StatelessWidget {
  static const routeName = "trainingProgramProvider";
  @override
  Widget build(BuildContext context) {
    DataBaseService databaseService = locator<DataBaseService>();
    Trainer trainer = ModalRoute.of(context).settings.arguments;
    return StreamProvider<List<TrainingProgram>>(
      create: (ctx) => databaseService.getTrainingPrograms(trainer.id),
      child: Container(child: SpecaficTrainer(trainer:trainer)),
    );
  }
}
