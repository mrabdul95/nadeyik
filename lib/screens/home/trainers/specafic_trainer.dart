import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:nadeyik/models/trainer.dart';
import 'package:nadeyik/models/trainingProgram.dart';
import 'package:nadeyik/screens/home/home.i18n.dart';
import 'package:provider/provider.dart';

class SpecaficTrainer extends StatelessWidget {
  final Trainer trainer;
  SpecaficTrainer({this.trainer});
  @override
  Widget build(BuildContext context) {
    String myLocale = Localizations.localeOf(context).languageCode;
    TextStyle header = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
    MediaQueryData mediaQuery = MediaQuery.of(context);
    List<TrainingProgram> _trainingPrograms =
        Provider.of<List<TrainingProgram>>(context) ?? [];

    final trainerAppBar = GradientAppBar(
      gradient: LinearGradient(
          colors: [Colors.blue[500], Colors.purple[300], Colors.pink[500]]),
      title: Text(myLocale == "en" ? trainer.name : trainer.nameAr),
    );
    return Scaffold(
      appBar: trainerAppBar,
      body: Container(
        height: mediaQuery.size.height,
        child: Column(
          children: <Widget>[
            Container(
                height: mediaQuery.size.height * 0.3,
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Card(
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          title: CircleAvatar(
                            radius: 50,
                            child: ClipOval(
                              clipBehavior: Clip.antiAlias,
                              child: Align(
                                heightFactor: 1,
                                widthFactor: 0.7,
                                child: Image.network(
                                  trainer.image,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          subtitle: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: myLocale == "en"
                                ? <Widget>[
                                    Text(
                                      trainer.name,
                                      style: header,
                                    ),
                                    // Text(trainer.gender,style: header,)
                                  ]
                                : <Widget>[
                                    Text(trainer.nameAr, style: header),
                                    // Text(trainer.gender.i18n,style: header,)
                                  ],
                          ),
                        ),
                        Divider(
                          color: Colors.black,
                          height: 1,
                          indent: 5,
                          endIndent: 5,
                        ),
                        Expanded(
                          child: Text(trainer.detailsAr),
                        )
                      ],
                    ),
                  ),
                )),
            SizedBox(
              height: 200,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: ListView.builder(
                  shrinkWrap: true,
                  itemBuilder: (ctx, index) => Card(
                    elevation: 5,
                    child: InkWell(
                      onTap: (){},
                      child: myLocale=="en"? ListTile(
                        title: Text(_trainingPrograms[index].name),
                        subtitle: Text(_trainingPrograms[index].description),
                        leading: Text(_trainingPrograms[index].difficulty<35?"easy".i18n:_trainingPrograms[index].difficulty<75?"Medium".i18n:"Hard".i18n,)
                      ): ListTile(),
                    ),
                  ),
                  itemCount: _trainingPrograms.length,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
