import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:nadeyik/models/trainer.dart';
import 'package:nadeyik/models/trainingProgram.dart';
import 'package:nadeyik/screens/home/trainers/training_program_tile.dart';
import 'package:nadeyik/services/database.dart';
import 'trainer_tile.dart';
import 'package:provider/provider.dart';
import 'package:nadeyik/screens/home/home.i18n.dart';

class TrainingProgramList extends StatelessWidget {
  static const routeName = "trainingProgramList";
  @override
  Widget build(BuildContext context) {
    List<TrainingProgram> _trainingPrograms= Provider.of<List<TrainingProgram>>(context)??[];
    print(_trainingPrograms);
    final trainerAppBar = GradientAppBar(
      gradient: LinearGradient(
          colors: [Colors.blue[500], Colors.purple[300], Colors.pink[500]]),
      title: Text("Training Programs".i18n),
    );

    
            return Scaffold(
              appBar: trainerAppBar,
              body: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 1,
                    childAspectRatio: 5 / 3,
                  ),
                  semanticChildCount: 5,
                  itemBuilder: (ctx, index) {
                    return _trainingPrograms == []
                        ? CircularProgressIndicator()
                        : Container(
                            margin: const EdgeInsets.symmetric(vertical: 10),
                            child: TrainingProgramTile(
                              trainingProgram:  _trainingPrograms[index],
                            ),
                          );
                  },
                  itemCount: _trainingPrograms.length,
                ),
              ),
            );
          
  }
}
