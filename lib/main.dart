import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:nadeyik/models/trainer.dart';
import 'package:nadeyik/models/user.dart';
import 'package:nadeyik/screens/authinticate_anon/authenticate_anon.dart';
import 'package:nadeyik/screens/home/gyms/gym_provider.dart';

import 'package:nadeyik/screens/home/gyms/gym_specific.dart';
import 'package:nadeyik/screens/home/trainers/trainer_provider.dart';
import 'package:nadeyik/screens/home/trainers/training_program_provider.dart';
import 'package:nadeyik/screens/home/trainers/training_programs_list.dart';

import 'package:nadeyik/screens/wrapper.dart';
import 'package:nadeyik/services/auth.dart';
import 'package:nadeyik/services/database.dart';
import 'package:nadeyik/services/purchase_iap.dart';
import 'package:provider/provider.dart';
import 'package:nadeyik/shared/shared_prefs.dart';

import 'services/service_locator.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  InAppPurchaseConnection.enablePendingPurchases();

  setUpLocator();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var sharedPrefs = SharedPrefs();

  bool initilize = true;
  bool isLoading = false;
  @override
  void initState() {
    // TODO: implement initState
    setState(() {
      sharedPrefs.getStoredLocale();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final databaseService = locator<DataBaseService>();
    return MultiProvider(
      providers: [
        Provider<AuthService>(
          create: (_) => AuthService(),
        ),
        ChangeNotifierProvider<DataBaseService>.value(value: databaseService),
        StreamProvider<List<Trainer>>(
            create: (ctx) => databaseService.trainers),
        ChangeNotifierProvider.value(
          value: sharedPrefs,
        ),
      ],
      child: Consumer<SharedPrefs>(
        builder: (ctx, model, child) => StreamProvider<FirebaseUser>.value(
          value: AuthService().user,
          child: PlatformApp(
              localizationsDelegates: [
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate
              ],
              locale: Locale(model.currentLocale),
              supportedLocales: [const Locale('en'), const Locale("ar")],
              title: 'Flutter Demo',
              android: (_)=>MaterialAppData(theme: ThemeData(
                // This is the theme of your application.
                //
                // Try running your application with "flutter run". You'll see the
                // application has a blue toolbar. Then, without quitting the app, try
                // changing the primarySwatch below to Colors.green and then invoke
                // "hot reload" (press "r" in the console where you ran "flutter run",
                // or simply save your changes to "hot reload" in a Flutter IDE).
                // Notice that the counter didn't reset back to zero; the application
                // is not restarted.
                primarySwatch: Colors.blue,
                fontFamily: "Mirza",
                // This makes the visual density adapt to the platform that you run
                // the app on. For desktop platforms, the controls will be smaller and
                // closer together (more dense) than on mobile platforms.
                visualDensity: VisualDensity.adaptivePlatformDensity,
              ),),
              
              home: false ? null : I18n(child: Wrapper()),
              routes: {
                GymSpecific.routeName: (ctx) => GymSpecific(),
                AuthenticateAnonScreen.routeName: (ctx) =>
                    AuthenticateAnonScreen(),
                GymProvider.routeName: (ctx) => GymProvider(),
                TrainerProvider.routeName: (ctx) => TrainerProvider(),
                TrainingProgramProvider.routeName: (ctx) =>
                    TrainingProgramProvider()
              }),
        ),
      ),
    );
  }
}
