// will hold our constantss, ie input decration styles and what not.TextStyle

import 'package:flutter/material.dart';

var outlineInputBorderStyle = OutlineInputBorder(
    borderSide: BorderSide(color: Color.lerp(Colors.purple[100], Colors.purple[400], 0.5), width: 2.0),
    borderRadius: BorderRadius.circular(10.0));
var textInputDecoration = InputDecoration(
  
    fillColor: Colors.white,
    filled: true,
    enabledBorder:
        OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
    errorBorder: outlineInputBorderStyle.copyWith(
        borderSide: BorderSide(color: Colors.red, width: 2.0),
        borderRadius: BorderRadius.circular(10.0)),
    focusedErrorBorder: outlineInputBorderStyle,
    focusedBorder: outlineInputBorderStyle,
    labelStyle: TextStyle(fontWeight: FontWeight.bold));
