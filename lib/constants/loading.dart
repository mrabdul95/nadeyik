import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      // margin:
      // EdgeInsets.only(top: mediaQuery.size.height * 0.03),
      height: 50.0,
      child: RaisedButton(
        onPressed: null,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
        padding: EdgeInsets.all(0.0),
        child: Ink(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Color(0xff374ABE), Colors.purple[500]],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
              ),
              borderRadius: BorderRadius.circular(30.0)),
          child: Container(
            constraints: BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
            alignment: Alignment.center,
            child: Center(
              child: SpinKitPumpingHeart(
                duration: Duration(seconds: 1),
                color: Colors.white,
                // size: 50.0,
              ),
            ),
          ),
        ),
      ),
    );

    //  Container(
    //   color: Colors.transparent,
    //   child: Center(
    //           child: SpinKitPumpingHeart(
    //             duration: Duration(seconds: 1),
    //       color: Colors.white,
    //       // size: 50.0,
    //     ),
    //   ),
    // );
  }
}
