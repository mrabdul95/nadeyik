import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs with ChangeNotifier {
  String _currentLocale ="en";

  String get currentLocale {
    return _currentLocale;
  }

  Future<void> getStoredLocale() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();

      if (prefs.containsKey("lang")) {
        if (prefs.getString("lang") == "en") {
          _currentLocale = "en";
          notifyListeners();
        } else {
          _currentLocale = "ar";
          notifyListeners();
        }
      } else {
        prefs.setString("lang", "en");
        _currentLocale = "en";
        notifyListeners();
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> toggleLocale() async {
    print("toggleLocal pressed");
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();

      if (prefs.containsKey("lang")) {
        if (prefs.getString("lang") == "en") {
          _currentLocale = "ar";
          prefs.setString("lang", "ar");
          notifyListeners();
        } else {
          _currentLocale = "en";
          prefs.setString("lang", "en");
          notifyListeners();
        }
      } else {
        prefs.setString("lang", "en");
        _currentLocale = "en";
        notifyListeners();
      }
    } catch (e) {
      print(e);
    }
  }
}
